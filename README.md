# Documentation

Below is link to the Sphinx Documentation from MAS514 projects in A2022.

Group1: https://mas514-group1.gitlab.io/documentation/

Group2: https://mas514-group2.gitlab.io/sphinx-rtd/  

Group3: https://mas514-group3.gitlab.io/documentation/

Group4: https://mas514-group7.gitlab.io/sphinx-rtd-group7/ 

Group5: https://uiabot.dundermifflin.no/ 

Main difference this year is the new Jetson Nano Orin 8gb and use of ROS2 Humble instead of ROS2 Galactic. 

Please make similar documentation for the MAS513 projects with updated components and objectives of your MAS513 UiAbot projects.
